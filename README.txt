INSTALLATION INSTRUCTIONS
-------------------------

1) Install the module as usual.

2) Obtain the necessary credentials from PRH to use the service

3) Visit the module settings at /admin/config/prh_search

After installation, the user functionality of the module can be found at /association_search
